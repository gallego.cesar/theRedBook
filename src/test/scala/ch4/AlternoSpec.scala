package ch4

import org.scalatest._

class AlternoSpec extends FunSpec with Matchers{
  describe("Alterno basicamente") {
    val media:(Seq[Double])=>Depende[String, Double] =
      (x) => if(x.isEmpty) Mal("Ta vacio") else Bien(x.sum / x.length)
    it("debe ser un valor que se devuelva en fucniones dudosas") {
      media(Seq()) shouldBe Mal("Ta vacio")
      media(Seq(1D)) shouldBe Bien(1D)
    }

  }
}