package ch4

import org.scalatest._

class OpcionalSpec extends FunSpec with Matchers{
  describe("Opcional basicamente") {
    val media:(Seq[Double])=>Opcional[Double] = (x) => if(x.isEmpty) Nada else Algo(x.sum / x.length)
    it("debe ser un valor que se devuelva en fucniones dudosas") {
      media(Seq()) shouldBe Nada
      media(Seq(1D)) shouldBe Algo(1D)
    }

    it("debe permitir el calculo combinado de elementos con flatMap") {
      val l = Seq(1D,2D,3D,4D)
      media(l).flatMap( m => Algo(l.map(x => math.pow(x - m, 2))) ) shouldBe Algo(List(2.25, 0.25, 0.25, 2.25))
    }

    it("debe permitir el calculo combinado de elemnetos incluso si la anterior computación no tuvo éxito") {
      val l = Seq.empty[Double]
      media(l).flatMap( m => Algo(l.map(x => math.pow(x - m, 2))) ) shouldBe Nada
    }

    it("debe permitir secuenciar una lista de opcionales en un opcional con una lista") {
      val l = Seq(Algo(1D),Algo(2D),Nada,Nada)

    }
  }
}