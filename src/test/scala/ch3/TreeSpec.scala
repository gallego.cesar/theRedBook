package ch3

import org.scalatest._

class TreeSpec extends FunSpec with Matchers{
  describe("the Tree"){
    it("Should return the correct size of elements") {
      val t = Branch(Leaf(1), Leaf(2))
      t.size shouldBe 2
    }

    it("Should return the maximum element") {
      import scala.math.Ordering.Int
      val t = Branch(Leaf(1), Leaf(2))
      t.maximum shouldBe 2
    }
  }
}
