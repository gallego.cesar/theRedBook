package ch3

import org.scalatest._

class ListaSpec extends FunSpec with Matchers{
  describe("The Lista basic operation"){
    it("Should construct by apply or by Elemento") {
      val elem = Elemento(1, Elemento(2, Elemento(3, Nada)))
      val lista = Lista(1,2,3)
      elem shouldBe lista
    }

    it("Should work with pattern Matching") {
      val x = Lista(1,2,3,4,5) match {
        case Elemento(x, Elemento(2, Elemento(4, _))) => x
        case Nada => 42
        case Elemento(x, Elemento(y, Elemento(3, Elemento(4, _)))) => x + y
        case Elemento(h, t) => h + Lista.sum(t)
        case _ => 101 }

      x shouldBe 3
    }

    it("Should return all elements but first when call tail") {
      val x = Lista(1,2,3,4,5)
      x.tail shouldBe Lista(2,3,4,5)
    }

    it("Should drop a number of elements") {
      val x = Lista(1,2,3,4,5)
      x.drop(2) shouldBe Lista(3,4,5)
    }

    it("Should drop while with function") {
      val x = Lista(1,2,3,4,5)
      x.dropWhile( x => x < 3) shouldBe Lista(3,4,5)
    }

    it("Should replace the first element on the Lista") {
      val x = Lista(1,2,3,4,5)
      x.setHead(6) shouldBe Lista(6,2,3,4,5)
    }

    it("Should return all elemnts but last when call init"){
      val x = Lista(1,2,3,4,5)
      x.init shouldBe Lista(1,2,3,4)
    }

    it("Should return the sum of List") {
      val x = Lista(1,2,3,4,5)
      Lista.sum(x) shouldBe 15
    }

    it("Should return the products of List") {
      val x:Lista[Double] = Lista(1D,2D,3D,4D,5D)
      Lista.product(x) shouldBe 120D
    }

    it("Should unchange the Lista whend fold over Lista elemnts") {
      val x = Lista(1,2,3)
      x.foldRight(Nada:Lista[Int])((acc, x) => Elemento(x,acc)) shouldBe Lista(1,2,3)
    }

    it("Should reverse the Lista whend fold over Lista elemnts") {
      val x = Lista(1,2,3)
      x.foldLeft(Nada:Lista[Int])((acc,x) => Elemento(x, acc)) shouldBe Lista(3,2,1)
    }

    it("Should change with map method") {
      val x = Lista(1,2,3)
      x.map(_.toString) shouldBe Lista("1","2","3")
    }

    it("Should filter to a new list") {
      val x = Lista(1,2,3)
      x.filter( _ % 2 == 0) shouldBe Lista(2)
    }

    it("Should flat a list") {
      val x = Lista(Lista(1,2),3)
      x.flattern shouldBe Lista(1,2,3)
    }

    it("Should concat two list") {
      val x = Lista(1,2)
      val y = Lista(3)
      x.concat(y) shouldBe Lista(1,2,3)
    }

    it("Should reverse correctly the list") {
      val l = Lista(1,2,3,4)
      l.reverse shouldBe Lista(4,3,2,1)
    }

    it("Should zip two list with a function") {
      val l1 = Lista[Int](1,2,3)
      val l2 = Lista[Int](4,5,6)
      val res:Lista[Int] = l1.zip(l2,(a:Int,b:Int) => a+b)

      res shouldBe Lista(5,7,9)
    }

    it("Should take n number of elements") {
      val l = Lista(1,2,3)
      l.take(2) shouldBe Lista(1,2)
    }

    it("Should take while the number is less than 3") {
      val l = Lista(1,2,3,4,5)
      l.takeWhile(x => x < 3) shouldBe Lista(1,2)
    }

    it("Should return true for all numbers under 100") {
      val l = Lista(1,2,3,4,5)
      l.forall(x => x<100) shouldBe true
    }

    it("Should return true if 4 exists") {
      val l = Lista(1,2,3,4,5)
      l.exists(x => x == 4) shouldBe true
    }
  }
}
