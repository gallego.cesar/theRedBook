package ch6

import org.scalatest._

class RNGSpec extends FunSpec with Matchers {
  describe("The integer absolute generator") {
    it("Should generate a random integer") {
      val rng = RNG.SimpleRNG(42)
      val (n, _) = RNG.int(rng)
      n should be > 0
    }

    it("Should generate a random double") {
      val rng = RNG.SimpleRNG(42)
      val (n,_) = RNG.double(rng)
      n should be > 0D
      n should be < 1D
    }

    it("Should generate a pair of Int and Double") {
      val rng = RNG.SimpleRNG(42)
      val (n,_) = RNG.intDouble()(rng)
      val (i, d) = n
      i shouldBe a[java.lang.Integer]
      d shouldBe a[java.lang.Double]
    }

    it("Should generate a pair of Double and Int") {
      val rng = RNG.SimpleRNG(42)
      val (n,_) = RNG.doubleInt()(rng)
      val (d, i) = n
      d shouldBe a[java.lang.Double]
      i shouldBe a[java.lang.Integer]
    }

    it("Should generate a triple of Double") {
      val rng = RNG.SimpleRNG(42)
      val (n,_) = RNG.double3()(rng)
      val (d1,d2,d3) = n
      d1 shouldBe a[java.lang.Double]
      d2 shouldBe a[java.lang.Double]
      d3 shouldBe a[java.lang.Double]
    }

    it("Should generate a list of 3 random ints") {
      val rng = RNG.SimpleRNG(42)
      val (n,_) = RNG.ints(3)(rng)
      n shouldBe a[List[Int]]
      n should have size 3
    }

    it("Should sequence a list of randoms to create a random of a list") {
      val rng = RNG.SimpleRNG(42)
      val l = List.fill(10)(RNG.double)
      val seq = RNG.sequence(l)
      val (n, _) = seq(rng)
      n shouldBe a[List[Double]]
      n should have size 10
    }

    it("Should generate a non negative number under 10000") {
      val rng = RNG.SimpleRNG(42)
      val (n,_) = RNG.nonNegativeLessThan(10000)(rng)
      n should be >= 0
      n should be < 10000
    }
  }
}
