package ch6

import org.scalatest._

class CandyMachineSpec extends FunSpec with Matchers {
  describe("The candy machine") {
    it("Should ignore all inputs if its blocked") {
      val inputs:List[Input] = List(Turn, Turn, Turn)
      val mach:Machine = Machine(locked = true, coins = 10, candies = 5)
      val state:State[Machine, (Int, Int)] = Machine.simulateMachine(inputs)
      val ((coins, candies), mach2) = state.run(mach)

      coins shouldBe 10
      candies shouldBe 5
      mach shouldBe mach2
    }

    it("Should be unlocked when input insert a coin") {
      val inputs:List[Input] = List(Coin)
      val mach:Machine = Machine(locked = true, coins = 10, candies = 5)
      val state:State[Machine, (Int, Int)] = Machine.simulateMachine(inputs)
      val ((coins, candies), mach2) = state.run(mach)

      coins shouldBe 10
      candies shouldBe 5
      mach2.locked shouldBe false
    }

    it("Should sell a candy") {
      val inputs:List[Input] = List(Coin, Turn)
      val mach:Machine = Machine(locked = true, coins = 10, candies = 5)
      val state:State[Machine, (Int, Int)] = Machine.simulateMachine(inputs)
      val ((coins, candies), mach2) = state.run(mach)

      coins shouldBe 11
      candies shouldBe 4
      mach2.locked shouldBe true
    }

    it("Should ignore all inputs when its out of candy") {
      val inputs:List[Input] = List(Coin, Turn, Coin, Turn, Coin, Turn, Coin, Turn)
      val mach:Machine = Machine(locked = true, coins = 10, candies = 0)
      val state:State[Machine, (Int, Int)] = Machine.simulateMachine(inputs)
      val ((_, _), mach2) = state.run(mach)

      mach shouldBe mach2
    }
  }
}
