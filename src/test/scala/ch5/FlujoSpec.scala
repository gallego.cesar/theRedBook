package ch5

import org.scalatest._

class FlujoSpec extends FunSpec with Matchers{
  describe("El flujo"){
    it("Debe poder convertirse en una lista") {
      val f = Flujo(1, 2, 3)
      f.toList shouldBe List(1,2,3)
    }

    it("Debe poder entregar n elementos") {
      val f = Flujo(1, 2, 3, 4, 5)
      f.take(3).toList shouldBe List(1,2,3)
    }

    it("Debe entregar elementos conforme a un predicado") {
      val f = Flujo(1, 2, 3, 4, 5)
      f.takeWhile( x => x < 4).toList shouldBe List(1,2,3)
    }
  }
}
