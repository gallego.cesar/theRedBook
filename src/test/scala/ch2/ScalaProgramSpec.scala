package ch2

import org.scalatest._

case class ScalaProgramSpec() extends FunSpec with Matchers {
  describe("The Scala program") {
    describe("Absolute"){
      it("format the absolute value") {
        val formatted = MyModule.format("absolute", -42, MyModule.abs)
        formatted shouldBe "The absolute value of -42 is 42"
      }
    }
    describe("Factorial"){
      it("format the factorial value") {
        val formatted = MyModule.format("factorial", 7, MyModule.factorial)
        formatted shouldBe "The factorial value of 7 is 5040"
      }
    }
    describe("isSorted"){
      it("return false for Array[Int](2,1,3)"){
        val sorted = MyModule.isSorted(Array(2,1,3), (a:Int,b:Int) => a > b)
        sorted shouldBe false
      }
      it("return true for Array[Int](1,2,3)"){
        val sorted = MyModule.isSorted(Array(1,2,3), (a:Int,b:Int) => a > b)
        sorted shouldBe true
      }
    }
  }
}
