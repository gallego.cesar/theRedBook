package ch2

import org.scalatest._

class FibonacciSpec extends FunSpec with Matchers {
  describe("The fibonacci sequence by position"){
    it("returns 0 for the 0 nth element"){
      Fibonacci.nth(0) shouldBe 0
    }
    it("returns 1 for the 1 nth element"){
      Fibonacci.nth(1) shouldBe 1
    }
    it("returns 1 for the 2 nth element"){
      Fibonacci.nth(2) shouldBe 1
    }
    it("returns 2 for the 3 nth element"){
      Fibonacci.nth(3) shouldBe 2
    }
    it("returns 5 for the 4 nth element"){
      Fibonacci.nth(4) shouldBe 3
    }
    it("returns 8 for the 5 nth element"){
      Fibonacci.nth(5) shouldBe 5
    }
    it("returns 13 for the 6 nth element"){
      Fibonacci.nth(6) shouldBe 8
    }
    it("returns 21 for the 7 nth element"){
      Fibonacci.nth(7) shouldBe 13
    }
    it("returns 34 for the 8 nth element"){
      Fibonacci.nth(8) shouldBe 21
    }
    it("returns 55 for the 9 nth element"){
      Fibonacci.nth(9) shouldBe 34
    }
  }
}
