/**
  * Created by cesar on 24/11/2016.
  */
package object ch6 {
  type Rand[+A] = RNG => (A, RNG)
}
