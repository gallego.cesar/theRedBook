package ch6

sealed trait Input
case object Coin extends Input
case object Turn extends Input

case class Machine(locked: Boolean, candies: Int, coins: Int)

object Machine {
  def applyState(currentMachine:Machine, input:Input):Machine = (currentMachine, input) match {
    case (Machine(_, 0, _), _) => currentMachine
    case (Machine(true,_,_), Turn) => currentMachine
    case (Machine(false,_,_), Coin) => currentMachine
    case (Machine(true, ca, c), Coin) => Machine(false, ca, c)
    case (Machine(false, ca, c), Turn) => Machine(true, ca-1, c+1)
  }

  def simulateMachine(inputs:List[Input]): State[Machine, (Int, Int)]={
    State[Machine, (Int, Int)]( machine => {
      val m = inputs.foldLeft(machine)(applyState)
      ((m.coins, m.candies), m)
    })
  }
}

