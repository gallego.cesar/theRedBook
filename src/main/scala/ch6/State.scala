package ch6

case class State[S,+A](run: S => (A, S)) {
  def map[B](f: A => B): S => (B,S) =
    seed => {
      val (a, s2) = run(seed)
      (f(a), s2)
    }

  def combine[B >: A,C,D](rb:State[S, C])(f: (B,C) => D):State[S, D]={
    State( (state:S) => {
      val (a, st2) = run(state)
      val (b, st3) = rb.run(st2)
      (f(a,b), st3)
    })
  }

  def flatMap[C](g: A => State[S, C]): State[S, C] = State( (state:S) => {
    val (x, state2) = run(state)
    g(x).run(state2)
  })

  /*
  def modify(f: S => S):State[S, Unit]= for {
    s <- get
    _ <- set(f(s))
  } yield ()

  def get:State[S,S] = State( s => (s,s) )
  def set(s:S):State[S,Unit] = State( _ => ((),s))
  */
}

object State {
  def unit[S, A](a:A):State[S, A]=
    State((s) => (a, s))

  def sequence[S, A](l:List[State[S, A]]):State[S, List[A]]= l match {
    case Nil => unit(Nil)
    case h :: t => h.combine(sequence(t))(_ :: _)
  }
}
