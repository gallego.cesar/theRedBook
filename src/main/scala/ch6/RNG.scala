package ch6

trait RNG {
  def nextInt: (Int, RNG)
}

object RNG {
  case class SimpleRNG(seed:Long) extends RNG {
    override def nextInt: (Int, RNG) = {
      val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
      val nextRNG = SimpleRNG(newSeed)
      val n = (newSeed >>> 16).toInt
      (n, nextRNG)
    }
  }

  val int:Rand[Int] = _.nextInt
  def double:Rand[Double] = map(int)((x) => x.toDouble / Int.MaxValue )

  def intDouble():Rand[(Int,Double)] = {
    both(int, double)
  }
  def doubleInt():Rand[(Double, Int)] = {
    both(double, int)
  }

  def double3():Rand[(Double, Double, Double)] = {
    val x = combine(double, double)((_,_))
    combine(x, double){
      (t, c) =>
        val (a,b) = t
        (a,b,c)
    }
  }

  def ints(count:Int):Rand[List[Int]] = {
    def loop(n:Int, r:Rand[List[Int]]):Rand[List[Int]]= n match {
      case 0 => r
      case _ => loop(n-1, combine(r, int)((l, x) => x :: l))
    }
    loop(count, unit(List()) )
  }

  def unit[A](a:A):Rand[A]=
    rng => (a, rng)

  def map[S,A,B](s: S => (A,S))(f: A => B): S => (B,S) =
    seed => {
      val (a, s2) = s(seed)
      (f(a), s2)
    }

  def nonNegativeEven: Rand[Int] =
    map(positiveInt)(i => i-i % 2)

  def positiveInt:Rand[Int]=
    (rng:RNG) => {
      val (n, next) = rng.nextInt
      if(n.abs <= 0) next.nextInt else ( n.abs, next )
    }

  def combine[A,B,C](ra:Rand[A], rb:Rand[B])(f: (A,B) => C):Rand[C]={
    rng => {
      val (a, rng2) = ra(rng)
      val (b, rng3) = rb(rng2)
      (f(a,b), rng3)
    }
  }

  def both[A,B](ra:Rand[A], rb:Rand[B]):Rand[(A,B)]=
    combine(ra,rb)((_,_))

  def sequence[A](l:List[Rand[A]]):Rand[List[A]]= l match {
    case Nil => unit(Nil)
    case h :: t => combine(h, sequence(t))(_ :: _)
  }

  def flatMap[A,B](f: Rand[A])(g: A => Rand[B]): Rand[B] = rng => {
    val (x, rng2) = f(rng)
    g(x)(rng2)
  }

  def nonNegativeLessThan(x:Int):Rand[Int]={
    rng => {
      flatMap(positiveInt)( i => {
        val mod = i % x
        if(i + (x-1) - mod > 0 ){
          unit(mod)
        }else{
          nonNegativeLessThan(x)
        }
      })(rng)
    }
  }
}
