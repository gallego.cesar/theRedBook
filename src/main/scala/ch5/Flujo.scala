package ch5

sealed trait Flujo[+A] {
  def toList: List[A] = this match {
    case Termina => Nil
    case Fluye(h, t) => h() :: t().toList
  }

  def take(n:Int):Flujo[A]= (n, this) match {
    case (_, Termina) => Termina
    case (0, _) => Termina
    case (x, Fluye(h,t)) => Fluye(h, () => t().take(x-1))
  }

  def takeWhile(f: A => Boolean):Flujo[A]=this match {
    case Termina => Termina
    case Fluye(h, t) if f(h()) => Fluye(h, () => t().takeWhile(f))
    case Fluye(_, _) => Termina
  }
}
case class Fluye[A](hd:() => A, tl:() => Flujo[A]) extends Flujo[A]
case object Termina extends Flujo[Nothing]

object Flujo{
  def apply[A](as : A*): Flujo[A] =
    if(as.isEmpty) Termina
    else Fluye(() => as.head, () => apply(as.tail: _*))

  def #::[A](as: A*) = apply(as)
}

//TODO: continuar en la pagina 70