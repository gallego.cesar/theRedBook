package ch3

sealed trait Lista[+A]{
  def tail: Lista[A] = this match {
    case Nada => Nada
    case Elemento(_, xs: Lista[A]) => xs
  }

  def drop(n:Int): Lista[A] = (n, this) match {
    case (_, Nada) => Nada
    case (1, Elemento(_, xs:Lista[A])) => xs
    case (x, Elemento(_, xs:Lista[A])) => xs.drop(x-1)
  }

  def dropWhile(f: A => Boolean): Lista[A] = this match {
    case Nada => Nada
    case Elemento(x, xs:Lista[A]) if !f(x) => this
    case Elemento(x, xs:Lista[A]) => xs.dropWhile(f)
  }

  def setHead[B >: A](a: B ): Lista[B] = this match {
    case Nada => Elemento(a, Nada)
    case Elemento(x, xs:Lista[B]) => Elemento[B](a, xs)
  }

  def init: Lista[A] = this match {
    case Nada => Nada
    case Elemento(x, Elemento(y, Nada)) => Elemento(x, Nada)
    case Elemento(x, xs) => Elemento(x, xs.init)
  }

  def foldRight[T](z:T)(f:(T,A) => T): T = this match {
    case Nada => z
    case Elemento(x, xs) => f(xs.foldRight(z)(f), x)
  }

  def foldLeft[T](z:T)(f:(T,A) => T): T = {
    def loop(l:Lista[A], acc:T): T = l match {
      case Nada => acc
      case Elemento(x, xs) => loop(xs, f(acc, x))
    }
    loop(this, z)
  }

  def append[B >: A](a:B):Lista[B]={
    Elemento(a, this)
  }

  def prepend[B >: A](a:B):Lista[B]= this match {
    case Nada => Elemento(a, Nada)
    case Elemento(x, Nada) => Elemento(x, Elemento(a, Nada))
    case Elemento(x, xs) => Elemento(x, xs.prepend(a))
  }

  def map[B](f: A => B):Lista[B]={
    this.foldRight(Nada:Lista[B])((acc:Lista[B], x:A) => acc.append(f(x)))
  }

  def concat[B >: A](ls:Lista[B]):Lista[B]=
    this.foldRight(ls)( (acc,x) => acc.append(x) )

  def flattern: Lista[A] = {
    def loop(ls:Lista[A]) :Lista[A] = ls match {
      case Nada => Nada
      case Elemento( e:Elemento[A], rs ) => e.flattern.concat(rs.flattern)
      case Elemento( a: A, as ) => Elemento(a, as.flattern)
    }
    loop(this)
  }

  def flatMap[B](f: A => Lista[B]): Lista[B] =  {
    this.foldLeft(Nada:Lista[B])((acc,x) =>{
      acc.concat(f(x))
    })
  }

  def filter(f: A => Boolean):Lista[A] = {
    this.flatMap( x => if(f(x)) Lista(x) else Lista() )
  }

  def size: Int = this.foldLeft(0)((acc, t) => acc + 1)

  def reverse: Lista[A] = {
    this.foldRight(Nada:Lista[A])((acc, x) => acc.prepend(x))
  }

  def zip[B,C](l:Lista[B], f:(A,B) => C):Lista[C]={
    require( this.size == l.size )

    def loop(l1:Lista[A], l2:Lista[B], acc:Lista[C]):Lista[C] = (l1, l2) match {
      case (Nada, _) => acc
      case (Elemento(a, as), Elemento(b, bs)) => loop(as, bs, acc.append( f(a,b) ))
    }

    loop(this, l, Nada:Lista[C]).reverse
  }

  def take(n:Int):Lista[A] = {
    def loop(n:Int, curr:Lista[A], acc:Lista[A]):Lista[A] = (n, curr) match {
      case (0, _) => acc
      case (x, Nada) => acc
      case (x, Elemento(e, es)) => loop(n-1, es, acc.append(e))
    }
    loop(n, this, Nada:Lista[A]).reverse
  }

  def takeWhile(f:A=>Boolean):Lista[A] = {
    def loop(curr:Lista[A], acc:Lista[A]):Lista[A] = curr match {
      case Nada => acc
      case Elemento(e, es) if !f(e) => acc
      case Elemento(e, es) => loop(es, acc.append(e))
    }
    loop(this, Nada:Lista[A]).reverse
  }

  def forall(f : A => Boolean):Boolean={
    this.foldLeft(true)((acc, x) => acc && f(x))
  }

  def exists(f : A=> Boolean):Boolean={
    def loop(l:Lista[A]):Boolean=l match {
      case Nada => false
      case Elemento(x, xs) if f(x) => true
      case Elemento(_, xs) => loop(xs)
    }
    loop(this)
  }
}
case object Nada extends Lista[Nothing]
case class Elemento[+A](head:A, rest:Lista[A]) extends Lista[A]

object Lista {

  def sum(ds:Lista[Int]): Int = {
    ds.foldLeft(0)(_ + _)
  }

  def product(ds:Lista[Double]):Double = {
    ds.foldLeft(1D)(_ * _)
  }

  def apply[A](as:A*):Lista[A]=
    if(as.isEmpty) Nada
    else Elemento(as.head, apply(as.tail: _*))
}
