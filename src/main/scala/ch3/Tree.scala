package ch3

sealed trait Tree[A]{
  def size:Int=this match {
    case Leaf(_) => 1
    case Branch(x,y) => x.size + y.size
  }

  def maximum(implicit ordering:Ordering[A]):A=this match {
    case Leaf(x) => x
    case Branch(x,y) => ordering.max(x.maximum, y.maximum)
  }

}

case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left:Tree[A], right: Tree[A]) extends Tree[A]

