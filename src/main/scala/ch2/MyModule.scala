package ch2

import scala.annotation.tailrec

object MyModule {
  def abs(x:Int):Int = if(x < 0) -x else x

  def factorial(x:Int):Int={
    @tailrec
    def go(n:Int, acc:Int):Int={
      if(n<=0) acc
      else go(n-1, acc*n)
    }

    go(x,1)
  }

  def format(name:String, x:Int, f: Int => Int)= s"The $name value of $x is ${f(x)}"

  def isSorted[T](as:Array[T], gt: (T,T) => Boolean): Boolean ={
    @tailrec
    def loop(i:Int, as:Array[T], last:Boolean): Boolean ={
      if( i == as.length) true
      else if( gt(as(i-1), as(i)) != last ) false
      else loop(i+1, as, last)
    }
    if(as.length > 1){
      loop(2, as, gt(as(0), as(1)))
    }else{
      true
    }
  }

  def partial1[A,B,C](a: A, f:(A,B) => C) : B => C = f(a,_)
  def curry[A,B,C](f:(A,B) => C) : A => (B => C) = (a:A) => f(a,_)
  def uncurry[A,B,C](f: A => B => C) : (A,B) => C = (a:A, b:B) => f(a)(b)
  def compose[A,B,C](f: B => C, g: A => B) : A => C = (a:A) => f(g(a))
}
