package ch2

import scala.annotation.tailrec

object Fibonacci {
  def nth(n:Int):Int = {

    @tailrec
    def fib(x:Int, a:Int, b:Int):Int={
      if(x <= 0) a
      else if(x < 2) b
      else fib(x-1, b, b+a)
    }

    fib(n, 0, 1)
  }
}
