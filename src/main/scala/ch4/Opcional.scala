package ch4


sealed trait Opcional[+A]{
  def map[B](f: A => B): Opcional[B]= this match {
    case Nada => Nada
    case Algo(x) => Algo(f(x))
  }

  def flatMap[B](f: A => Opcional[B]): Opcional[B]=this match{
    case Nada => Nada
    case Algo(x) => f(x)
  }

  def getOrElse[B >: A](x: => B):Opcional[B]=this match {
    case Nada => Algo(x)
    case Algo(y) => Algo(y)
  }

  def orElse[B >: A](ob: => Opcional[B]):Opcional[B]=this match {
    case Nada => ob
    case x:Algo[A] => x
  }

  def filter(f: A => Boolean):Opcional[A]=this match{
    case Nada => Nada
    case Algo(x) if f(x) => Algo(x)
    case _ => Nada
  }

  def isEmpty: Boolean=this match {
    case Nada => true
    case _ => false
  }
}
case class Algo[+A](get:A) extends Opcional[A]
case object Nada extends Opcional[Nothing]

object Opcional{
  def compose[A,B,C](a:Opcional[A], b:Opcional[B])(f:(A,B) => C)={
    a.flatMap( aa => b.map( bb => f(aa,bb)))
  }

  def traverse[A, B](a:List[A])(f: A => Opcional[B]):Opcional[List[B]]=a match{
    case Nil => Algo(Nil)
    case h :: t => compose(f(h), traverse(t)(f))( _ :: _ )
  }
}