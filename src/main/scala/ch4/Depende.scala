package ch4

sealed trait Depende[+E, +A]{
  def map[B](f: A=>B):Depende[E,B]=this match {
    case Mal(x) => Mal(x)
    case Bien(x) => Bien(f(x))
  }
  def flatMap[EE >: E, B](f: A=>Depende[EE,B]):Depende[EE,B]=this match {
    case Mal(x) => Mal(x)
    case Bien(x) => f(x)
  }
  def orElse[EE >: E, B >: A](b: => Depende[EE,B]):Depende[EE,B]=this match {
    case Mal(x) => b
    case _ => this
  }
  def componer[EE >: E, B, C](b: Depende[EE,B])(f: (A,B) => C):Depende[EE,C]=this match {
    case Mal(x) => Mal(x)
    case _ => this.flatMap( xx => b.flatMap( bb => Bien(f(xx,bb))))
  }
}
case class Mal[+E](valor:E) extends Depende[E, Nothing]
case class Bien[+A](valor:A) extends Depende[Nothing, A]

object Depende {
  def sequence[E,A](a:List[Depende[E,A]]):Depende[E, List[A]] = a match {
    case Nil => Bien(Nil)
    case h :: t => h.componer( sequence(t) )(_ :: _)
  }

  def traverse[E,A,B](a:List[A])(f: A => Depende[E,B]):Depende[E, List[B]]= a match {
    case Nil => Bien(Nil)
    case h :: t => f(h).componer(traverse(t)(f))(_ :: _)
  }
}